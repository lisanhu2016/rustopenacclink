# Rust Link with OpenACC shared library Sample

## Overview
This repo is to show how to link Rust driver code with OpenACC GPU code and make it run. The sample code is a simple implementation of 2-d Matrix Multiplication function. Rust driver will generate 2 random matrices with M * N and N * P as dimensions, then it will call the OpenACC kernel to compute the results. The results will be displayed with the ndarray Rust library, and the results will be verified using ndarray dot product.

The files mentioned in the following sections will be first represented with full path that the root is the root of this repository. Then the files will be refered by their names only. 

## Major Steps and Concerns to Create a Rust code Linking with OpenACC code (Using NVHPC)
1. Create the C code `/OpenACCKernel/matmul.c`, and its interface file `/OpenACCKernel/matmul.h`
2. Using `bindgen` rust tool to generate the binding rust file `/RustDriver/src/bindings.rs` or you can do it yourself
    * You can remove the lines that are not used in this code, for example, the constants and the test code generated with `bindgen`
3. Adding attributes line to the `bindings.rs` file (stating the library name which is a dylib)
4. Finsh the building script for your C code
    * If you are using NVHPC compiler, remember to add `-shared -acc` for your target_compile_options, and add `-acc` for your target_link_options.
    * It's better to install your shared library to your Rust code `release` folder and its `deps` sub-directory, in our case, it's `/RustDriver/target/release/deps`. 
        + The reason for this is when we run the code with `cargo run --release`, it will add the release `deps` folder as `LD_LIBRARY_PATH`, and thus we don't need to manually add it.
        + If you don't want to do this, please remember to add the folder where the shared library is installed to your `LD_LIBRARY_PATH` environment variable
5. Finish writing your Rust code
6. Configure your Rust build with `/RustDriver/build.rs`
    * For this step, you will need to make sure `nvc` is in your `PATH`, the build script in this sample code will call `which` command to get the path to the `nvhpc` lib folder, we have to link our code with the runtime there.
    * We will need the `LD_LIBRARY_PATH` to contain the lib folder for `nvhpc` to correctly run the code, because the `nvhpc` runtime has to be loaded when running the code.

## Build and Run script
```bash
# NVHPC compiler required in the PATH environment variable 
# and lib directory required in the LD_LIBRARY_PATH environment variable
module add nvhpc/21.7

cd OpenACCKernel; mkdir build
cd build; cmake ../ ; make install; cd ../../RustDriver
cargo run --release # this will automatically compile the code and then run it
```
Remeber that calling `target/release/RustDriver` will encounter error because we need `libmatmul.so`'s parent directory in `LD_LIBRARY_PATH`. Or we can manually add the parent directory of `libmatmul.so` to `LD_LIBRARY_PATH` and then call `target/release/RustDriver`

### Sample Output (The results may change because we are doing random matrices)
```
(base) lisanhu@skywalker:~/tmp/rustopenacclink/RustDriver
$ cargo run --release
   Compiling RustDriver v0.1.0 (/usa/lisanhu/tmp/rustopenacclink/RustDriver)
warning: /opt/nvidia/hpc_sdk/21.5/Linux_x86_64/21.5/compilers/lib
    Finished release [optimized] target(s) in 1.33s
     Running `target/release/RustDriver`
Begin calling OpenACC kernel
Begin computation...
OpenACC Matrix result is:
[[1599, 1230, 1043, 1417, 1266, ..., 1312, 1275, 1455, 1093, 1581],
 [1118, 1463, 1207, 1320, 996, ..., 1118, 1162, 1426, 1008, 1281],
 [1290, 1738, 1646, 1456, 1429, ..., 1282, 1089, 1354, 1309, 893],
 [1565, 1278, 1614, 1026, 1570, ..., 1249, 997, 1271, 829, 1171],
 [1045, 1584, 1343, 1080, 1037, ..., 945, 971, 1312, 1078, 1160],
 ...,
 [1771, 1042, 1316, 1135, 1317, ..., 1123, 1127, 896, 1127, 1352],
 [1423, 1076, 1384, 829, 1466, ..., 727, 936, 1127, 909, 1143],
 [1411, 1224, 1409, 768, 1393, ..., 772, 1426, 1319, 1552, 1416],
 [1218, 1086, 890, 1138, 1389, ..., 1269, 1301, 1490, 1339, 1287],
 [1325, 1620, 1426, 1347, 1206, ..., 1535, 1111, 1141, 1276, 1522]]
Time taken: 1.123762822 seconds

ndarray Matrix result is:
[[1599, 1230, 1043, 1417, 1266, ..., 1312, 1275, 1455, 1093, 1581],
 [1118, 1463, 1207, 1320, 996, ..., 1118, 1162, 1426, 1008, 1281],
 [1290, 1738, 1646, 1456, 1429, ..., 1282, 1089, 1354, 1309, 893],
 [1565, 1278, 1614, 1026, 1570, ..., 1249, 997, 1271, 829, 1171],
 [1045, 1584, 1343, 1080, 1037, ..., 945, 971, 1312, 1078, 1160],
 ...,
 [1771, 1042, 1316, 1135, 1317, ..., 1123, 1127, 896, 1127, 1352],
 [1423, 1076, 1384, 829, 1466, ..., 727, 936, 1127, 909, 1143],
 [1411, 1224, 1409, 768, 1393, ..., 772, 1426, 1319, 1552, 1416],
 [1218, 1086, 890, 1138, 1389, ..., 1269, 1301, 1490, 1339, 1287],
 [1325, 1620, 1426, 1347, 1206, ..., 1535, 1111, 1141, 1276, 1522]]
Time taken: 31.25117342 seconds


GPU results and CPU results are the same

```

## Profiling with NCU
Please refer to `/RustDriver/run-ncu.sh` or the following line
```
ncu --target-processes all --details-all -o prof cargo run --release
```
Here, `--target-processes all` is required because `cargo` will be calling our code as `sub-process`
Please refer to NVIDIA NSIGHT COMPUTE documentation for other flags