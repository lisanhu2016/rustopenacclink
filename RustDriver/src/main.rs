#![allow(non_snake_case)]

include!("./bindings.rs");

use ndarray::{rcarr1};

fn fill(mat: &mut Vec<i32>, rng: &mut impl rand::Rng) {
    for v in mat {
        *v = rng.gen_range(-3..3);
    }
}

fn main() {
    let M: usize = 3_000;
    let N: usize = 5_000;
    let P: usize = 2_000;
    let mut A = Vec::<i32>::with_capacity(M * N);
    let mut B = Vec::<i32>::with_capacity(N * P);
    let mut C = Vec::<i32>::with_capacity(M * P);
    A.resize(M * N, 0);
    B.resize(N * P, 0);
    C.resize(M * P, 0);
    let mut rng = rand::thread_rng();
    fill(&mut A, &mut rng);
    fill(&mut B, &mut rng);

    println!("Begin calling OpenACC kernel");
    let begin = std::time::Instant::now();
    unsafe {
        MatrixMultiplication(
            A.as_mut_ptr(),
            M as u32,
            N as u32,
            B.as_mut_ptr(),
            P as u32,
            C.as_mut_ptr(),
        )
    };
    let elapse = begin.elapsed().as_secs_f64();
    let C = rcarr1(&C);
    let CC = C.reshape((M, P));
    println!("OpenACC Matrix result is:\n{}", CC);
    println!("Time taken: {} seconds\n", elapse);

    let AA = rcarr1(&A);
    let BB = rcarr1(&B);
    let AA = AA.reshape((M, N));
    let BB = BB.reshape((N, P));

    let begin = std::time::Instant::now();
    let ret = AA.dot(&BB);
    let elapse = begin.elapsed().as_secs_f64();
    println!("ndarray Matrix result is:\n{}", ret);
    println!("Time taken: {} seconds\n", elapse);
    if CC == ret {
        println!("\nGPU results and CPU results are the same\n");
    } else {
        println!("\nGPU results and CPU results are different\n");
    }
}
