use std::env;

fn search_dir(path: &str) {
    println!("cargo:rustc-link-search={}", path); // the "-L" flag
}

fn link_lib(lib_name: &str) {
    println!("cargo:rustc-link-lib={}", lib_name);
}

fn update_on(path: &str) {
    println!("cargo:rerun-if-changed={}", path);
}

pub fn warn(text: &str) {
    println!("cargo:warning={}", text);
}

fn main() {
    //  -L for the C library
    let project_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let mut path = std::path::PathBuf::from(project_dir);
    path.push("../install/lib");
    search_dir(path.to_str().unwrap());

    //  -L for NVHPC compiler runtime libs
    let out = String::from_utf8(
        std::process::Command::new("which")
            .arg("nvc")
            .output()
            .unwrap().stdout
    ).unwrap();
    let mut nvhpc_home = std::path::PathBuf::from(out);
    nvhpc_home.pop();
    nvhpc_home.pop();
    let nvhpc_lib = nvhpc_home.join("lib");
    let nvhpc_lib = nvhpc_lib.to_str().unwrap();
    warn(nvhpc_lib);
    search_dir(nvhpc_lib);
    link_lib("acchost");

    update_on(path.join("libmatmul.so").to_str().unwrap());
}
