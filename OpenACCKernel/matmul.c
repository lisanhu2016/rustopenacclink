#include <stdio.h>
#include <stdlib.h>

#include "matmul.h"

size_t _idx(size_t r, size_t c, size_t w) { return r * w + c; }

/**
 *  Matrix Multiplication function, taking A, B as input, C as output
 *  A: (M, N)
 *  B: (N, P)
 *  C: (M, P)
 */
int MatrixMultiplication(int *const MatA, uint32_t A_h, uint32_t A_w, int *const MatB, uint32_t B_w, int *MatC) {
    uint32_t B_h = A_w;

    printf("Begin computation...\n");
#pragma acc data copyout(MatC[:A_h * B_w]) copyin(MatA[:A_w * A_h], MatB[:B_w * B_h])
    {
#pragma acc parallel loop collapse(2)
        for (size_t i = 0; i < A_h; i++) {
            for (size_t j = 0; j < B_w; j++) {
                int tmp = 0;
#pragma acc loop vector reduction(+ : tmp)
                for (size_t k = 0; k < A_w; k++) {
                    tmp += MatA[_idx(i, k, A_w)] * MatB[_idx(k, j, B_w)];
                }
                MatC[_idx(i, j, B_w)] = tmp;
            }
        }
    }

    return 0;
}
