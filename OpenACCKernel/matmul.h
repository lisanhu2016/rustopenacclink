#include <stdint.h>

/**
 *  Matrix Multiplication function, taking A, B as input, C as output
 *  A: (M, N)
 *  B: (N, P)
 *  C: (M, P)
 */
int MatrixMultiplication(int *MatA, uint32_t A_h, uint32_t A_w, int *MatB, uint32_t B_w, int *MatC);
